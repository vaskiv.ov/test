#!/bin/sh

rm -rf ./var/cache && rm -rf ./var/log
mkdir -p ./var/cache && mkdir -p ./var/log && mkdir -p ./var/log

composer install --prefer-dist --no-dev --no-scripts --no-progress \
    && composer dump-autoload --classmap-authoritative --no-dev \
    && composer dump-env prod \
    && composer run-script --no-dev post-install-cmd

php bin/console d:m:m -n

chmod -R 0775 ./var && chown -R www-data:www-data ./var

php bin/console lexik:jwt:generate-keypair --skip-if-exists

chmod -R 0775 ./config/jwt

exec "$@"