#!/bin/sh

rm -rf ./var/cache && rm -rf ./var/log
mkdir -p ./var/cache && mkdir -p ./var/log && mkdir -p ./var/log

composer install --no-suggest --optimize-autoloader --no-interaction

php bin/console d:m:m -n
php bin/console d:f:l -n

chmod -R 0775 ./var && chown -R www-data:www-data ./var

php bin/console lexik:jwt:generate-keypair --skip-if-exists

chmod -R 0775 ./config/jwt

exec "$@"