<?php declare(strict_types=1);

namespace App\Event;

use App\Service\Attribute\AttributeReader;
use App\Service\Cache\Attribute\CacheClear;
use App\Service\Cache\Attribute\CachedResponse;
use App\Service\Cache\CacheService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CachedResponseSubscriber implements EventSubscriberInterface
{
    private ?string $cacheKey = null;
    private ?array $cacheTags = [];
    private ?array $tagsToClear = [];
    private bool $isCachedResponse = false;

    public function __construct(
        private readonly CacheService $cache,
        private readonly AttributeReader $attributeReader
    ) {}

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => array(
                array('onKernelController'),
            ),
            KernelEvents::RESPONSE => array(
                array('onKernelResponse'),
            )
        );
    }

    public function onKernelController(ControllerEvent $event)
    {
        if (!is_array($event->getController()) || !$event->isMainRequest()) {
            return;
        }

        [$controller, $method] = $event->getController();

        /** @var ?CachedResponse $cacheAttribute */
        $cacheAttribute = $this->attributeReader->getMethodAttribute(
            $controller, $method, CachedResponse::class
        );

        if ($cacheAttribute) {
            $this->cacheKey = $cacheAttribute->getKey() ?? null;
            $this->cacheTags = $cacheAttribute->getTags() ?? [];

            if ($this->cacheKey) {
                $routeParams = $event->getRequest()->attributes->get('_route_params');
                $queryParams = $event->getRequest()->query->all();
                $params = array_merge($routeParams, $queryParams);

                if (is_array($params) && count($params)) {
                    $this->cacheKey .= '_' . md5(serialize($params));
                }

                $cachedResponse = $this->cache->get($this->cacheKey);
                if ($cachedResponse) {
                    $this->isCachedResponse = true;
                    $event->setController(fn() => new Response($cachedResponse, Response::HTTP_OK));
                }
            }
        }

        /** @var ?CacheClear $cacheClearAttribute */
        $cacheClearAttribute = $this->attributeReader->getMethodAttribute(
            $controller, $method, CacheClear::class
        );
        if ($cacheClearAttribute) {
            $this->tagsToClear = $cacheClearAttribute->getTags() ?? [];
        }
    }

    public function onKernelResponse(ResponseEvent $event)
    {
        if (count($this->tagsToClear) > 0) {
            $this->cache->deleteByTags($this->tagsToClear);
            $this->isCachedResponse = false;
        }

        if (
            $event->getResponse()->getStatusCode() == Response::HTTP_OK &&
            !$this->isCachedResponse &&
            $this->cacheKey &&
            !$this->cache->has($this->cacheKey)
        ) {
            $this->cache->set($this->cacheKey, $event->getResponse()->getContent(), $this->cacheTags);
        }
    }
}
