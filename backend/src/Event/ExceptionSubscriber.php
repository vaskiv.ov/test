<?php declare(strict_types=1);

namespace App\Event;

use App\Service\Exception\ExceptionHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Messenger\Exception\HandlerFailedException;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly ExceptionHandler $exceptionHandler
    ) {}

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => array(
                array('onKernelException')
            )
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof HandlerFailedException) {
            $exception = $exception->getNestedExceptions()[0];
        }

        $exceptionResponse = $this->exceptionHandler->handle($exception);

        $event->setResponse(new JsonResponse($exceptionResponse, $exceptionResponse->getHttpCode()));
    }
}