<?php declare(strict_types=1);

namespace App\Service\Cache;

use Symfony\Contracts\Cache\TagAwareCacheInterface;

class CacheService
{
    public function __construct(
        private readonly TagAwareCacheInterface $cache
    ) {}

    public function get(string $key): mixed
    {
        $item = $this->cache->getItem($key);
        if (!$item->isHit()) {
            return null;
        }

        return $item->get();
    }

    public function set(string $key, mixed $data, array $tags = []): void
    {
        $item = $this->cache->getItem($key);
        $item->set($data);
        if (count($tags) > 0) {
            $item->tag($tags);
        }
        $item->expiresAfter((int)$_ENV['CACHE_TTL']);
        $this->cache->save($item);
    }

    public function has(string $key): mixed
    {
        return $this->cache->hasItem($key);
    }

    public function delete(string $key): bool
    {
        return $this->cache->deleteItem($key);
    }

    public function deleteByTags(array $tags): bool
    {
        return $this->cache->invalidateTags($tags);
    }
}
