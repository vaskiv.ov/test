<?php declare(strict_types=1);

namespace App\Service\Cache\Attribute;

/**
 * View annotation class.
 *
 * @Annotation
 * @Target({"METHOD"})
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
class CachedResponse
{
    public function __construct(
        private string $key,
        private array $tags = []
    ) {}

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }
}
