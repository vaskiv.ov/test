<?php declare(strict_types=1);

namespace App\Service\Cache\Attribute;

/**
 * View annotation class.
 *
 * @Annotation
 * @Target({"METHOD"})
 */
#[\Attribute(\Attribute::TARGET_METHOD)]
class CacheClear
{
    public function __construct(
        private array $tags = []
    ) {}

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }
}
