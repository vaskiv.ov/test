<?php declare(strict_types=1);

namespace App\Service\ParamConverter;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class QueryParamConverter implements ParamConverterInterface
{
    /** @var Serializer */
    private $serializer;

    /** @var array */
    private $context = [];

    /**
     * @param Serializer $serializer
     * @param array|null $groups An array of groups to be used in the serialization context
     * @param string|null $version A version string to be used in the serialization context
     */
    public function __construct(
        Serializer $serializer,
        $groups = null,
        $version = null
    )
    {
        $this->serializer = $serializer;

        if (!empty($groups)) {
            $this->context['groups'] = (array)$groups;
        }

        if (!empty($version)) {
            $this->context['version'] = $version;
        }
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        $options = (array)$configuration->getOptions();

        if (isset($options['deserializationContext']) && is_array($options['deserializationContext'])) {
            $arrayContext = array_merge($this->context, $options['deserializationContext']);
        } else {
            $arrayContext = $this->context;
        }
        $this->configureContext($context = new Context(), $arrayContext);

        $queryString = json_encode($request->query->all());

        if (!$request->getContentType()) {
            $request->headers->set('Content-Type', 'application/json');
        }

        try {
            $object = $this->serializer->deserialize(
                $queryString,
                $configuration->getClass(),
                $request->getContentType(),
                $context
            );
        } catch (\Exception $exception) {
            throw $exception;
        }

        $request->attributes->set($configuration->getName(), $object);
    }

    public function supports(ParamConverter $configuration)
    {
        return null !== $configuration->getClass() && 'query_param_converter' === $configuration->getConverter();
    }

    /**
     * @param Context $context
     * @param array $options
     */
    protected function configureContext(Context $context, array $options)
    {
        foreach ($options as $key => $value) {
            if ('groups' === $key) {
                $context->addGroups($options['groups']);
            } elseif ('version' === $key) {
                $context->setVersion($options['version']);
            } elseif ('enableMaxDepth' === $key) {
                $context->enableMaxDepth($options['enableMaxDepth']);
            } elseif ('serializeNull' === $key) {
                $context->setSerializeNull($options['serializeNull']);
            } else {
                $context->setAttribute($key, $value);
            }
        }
    }
}
