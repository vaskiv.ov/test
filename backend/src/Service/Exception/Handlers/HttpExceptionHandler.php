<?php declare(strict_types=1);

namespace App\Service\Exception\Handlers;

use App\DTO\ExceptionResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

final class HttpExceptionHandler implements ExceptionHandlerInterface
{
    public function supports(\Throwable $exception): bool
    {
        return $exception instanceof HttpExceptionInterface;
    }

    public function handle(\Throwable $exception): ExceptionResponse
    {
        return new ExceptionResponse($exception->getStatusCode(), $exception->getMessage());
    }
}