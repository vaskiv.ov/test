<?php declare(strict_types=1);

namespace App\Service\Exception\Handlers;

use App\DTO\ExceptionResponse;

interface ExceptionHandlerInterface
{
    public function supports(\Throwable $exception): bool;

    public function handle(\Throwable $exception): ExceptionResponse;
}