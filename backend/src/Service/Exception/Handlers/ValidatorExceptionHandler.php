<?php declare(strict_types=1);

namespace App\Service\Exception\Handlers;

use App\DTO\ExceptionResponse;
use App\Exception\ConstraintValidatorException;
use Symfony\Component\HttpFoundation\Response;

final class ValidatorExceptionHandler implements ExceptionHandlerInterface
{
    public function supports(\Throwable $exception): bool
    {
        return $exception instanceof ConstraintValidatorException;
    }

    public function handle(\Throwable $exception): ExceptionResponse
    {
        return new ExceptionResponse(
            Response::HTTP_BAD_REQUEST,
            $exception->getMessage(),
            $this->getValidatorDetails($exception)
        );
    }

    private function getValidatorDetails(ConstraintValidatorException $exception): array
    {
        $details = [];
        foreach ($exception->getConstraintViolationList() as $violation) {
            $details[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return $details;
    }
}