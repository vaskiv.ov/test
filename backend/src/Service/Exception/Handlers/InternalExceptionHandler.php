<?php declare(strict_types=1);

namespace App\Service\Exception\Handlers;

use App\DTO\ExceptionResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

final class InternalExceptionHandler implements ExceptionHandlerInterface
{
    public function __construct(
        private readonly LoggerInterface $logger
    ) {}

    public function supports(\Throwable $exception): bool
    {
        return true;
    }

    public function handle(\Throwable $exception): ExceptionResponse
    {
        $this->logger->critical($exception->getMessage());

        return new ExceptionResponse(Response::HTTP_INTERNAL_SERVER_ERROR, "Internal error!");
    }
}