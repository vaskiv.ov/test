<?php declare(strict_types=1);

namespace App\Service\Exception;

use App\DTO\ExceptionResponse;
use App\Service\Exception\Handlers\InternalExceptionHandler;

final class ExceptionHandler
{
    public function __construct(
        private readonly array $handlers,
        private readonly InternalExceptionHandler $internalExceptionHandler,
    ) {}

    public function handle(\Throwable $exception): ExceptionResponse
    {
        foreach ($this->handlers as $handler) {
            if ($handler->supports($exception)) {
               return $handler->handle($exception);
            }
        }

        return $this->internalExceptionHandler->handle($exception);
    }
}