<?php declare(strict_types=1);

namespace App\Service\Attribute;

class AttributeReader
{
    public function getMethodAttribute(
        object|string $object,
        string $method,
        string $attribute
    ): mixed
    {
        $reflectedMethod = new \ReflectionMethod($object, $method);
        /** @var ?\ReflectionAttribute $cacheAttribute */
        $reflectionAttribute = current($reflectedMethod->getAttributes($attribute));
        if ($reflectionAttribute) {
            $name = $reflectionAttribute->getName();
            $arguments = $reflectionAttribute->getArguments();

            return new $name(...$arguments);
        }

        return null;
    }
}