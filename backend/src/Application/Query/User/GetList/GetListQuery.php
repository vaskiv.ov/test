<?php declare(strict_types=1);

namespace App\Application\Query\User\GetList;

use App\DTO\QueryParams;

final class GetListQuery
{
    public function __construct(
        private readonly QueryParams $queryParams
    ) {}

    public function getQueryParams(): QueryParams
    {
        return $this->queryParams;
    }
}
