<?php declare(strict_types=1);

namespace App\Application\Query\User\GetList;

use App\DTO\User\ListResponse;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler()]
final class GetListQueryHandler
{
    public function __construct(
        private readonly UserRepository $userRepository
    ) {}

    public function __invoke(GetListQuery $query): ListResponse
    {
        return $this->userRepository->findAllPaginated($query->getQueryParams());
    }
}
