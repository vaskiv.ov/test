<?php declare(strict_types=1);

namespace App\Application\Command\User\Create;

use App\DTO\User\CreateUserRequest;

final class CreateCommand
{
    public function __construct(
        private readonly CreateUserRequest $request
    ) {}

    public function getRequest(): CreateUserRequest
    {
        return $this->request;
    }
}
