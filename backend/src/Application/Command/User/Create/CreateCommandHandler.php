<?php declare(strict_types=1);

namespace App\Application\Command\User\Create;

use App\Entity\User;
use App\Exception\User\UserAlreadyExistsException;
use App\Factory\User\UserFactory;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler()]
final class CreateCommandHandler
{
    public function __construct(
        private readonly UserFactory $userFactory,
        private readonly UserRepository $userRepository
    ) {}

    public function __invoke(CreateCommand $command): User
    {
        if ($this->userRepository->loadUserByIdentifier($command->getRequest()->email)) {
            throw new UserAlreadyExistsException();
        }

        $user = $this->userFactory->updateFromRequest(new User(), $command->getRequest());
        $this->userRepository->save($user);

        return $user;
    }
}
