<?php declare(strict_types=1);

namespace App\Application\Command\User\Delete;

use App\Repository\UserRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler()]
final class DeleteCommandHandler
{
    public function __construct(
        private readonly UserRepository $userRepository
    ) {}

    public function __invoke(DeleteCommand $command): void
    {
        $this->userRepository->delete($command->getUser());
    }
}
