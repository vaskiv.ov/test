<?php declare(strict_types=1);

namespace App\Application\Command\User\Delete;

use App\Entity\User;

final class DeleteCommand
{
    public function __construct(
        private readonly User $user
    ) {}

    public function getUser(): User
    {
        return $this->user;
    }
}
