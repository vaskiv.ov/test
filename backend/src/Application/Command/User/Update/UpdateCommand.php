<?php declare(strict_types=1);

namespace App\Application\Command\User\Update;

use App\DTO\User\UpdateUserRequest;
use App\Entity\User;

final class UpdateCommand
{
    public function __construct(
        private readonly User $user,
        private readonly UpdateUserRequest $request
    ) {}

    public function getUser(): User
    {
        return $this->user;
    }

    public function getRequest(): UpdateUserRequest
    {
        return $this->request;
    }
}
