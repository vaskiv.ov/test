<?php declare(strict_types=1);

namespace App\Application\Command\User\Update;

use App\Entity\User;
use App\Factory\User\UserFactory;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler()]
final class UpdateCommandHandler
{
    public function __construct(
        private readonly UserFactory $userFactory,
        private readonly UserRepository $userRepository
    ) {}

    public function __invoke(UpdateCommand $command): User
    {
        $user = $this->userFactory->updateFromRequest($command->getUser(), $command->getRequest());
        $this->userRepository->save($user);

        return $user;
    }
}
