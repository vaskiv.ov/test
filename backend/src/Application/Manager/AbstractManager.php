<?php declare(strict_types=1);

namespace App\Application\Manager;

use App\Exception\ConstraintValidatorException;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractManager
{
    use HandleTrait { handle as protected; }

    public function __construct(
        MessageBusInterface $messageBus,
        protected readonly ValidatorInterface $validator
    )
    {
        $this->messageBus = $messageBus;
    }

    public function validate(mixed $request): bool
    {
        $constraintViolationList = $this->validator->validate($request);
        if ($constraintViolationList->count()) {
            throw new ConstraintValidatorException($constraintViolationList);
        }

        return true;
    }
}
