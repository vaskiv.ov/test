<?php declare(strict_types=1);

namespace App\Application\Manager;

use App\Application\Command\User\Create\CreateCommand;
use App\Application\Command\User\Delete\DeleteCommand;
use App\Application\Command\User\Update\UpdateCommand;
use App\Application\Query\User\GetList\GetListQuery;
use App\DTO\QueryParams;
use App\DTO\User\CreateUserRequest;
use App\DTO\User\ListResponse;
use App\DTO\User\UpdateUserRequest;
use App\Entity\User;

final class UserManager extends AbstractManager
{
    public function getList(QueryParams $queryParams): ListResponse
    {
        $this->validate($queryParams);

        return $this->handle(
            new GetListQuery($queryParams)
        );
    }

    public function create(CreateUserRequest $request): User
    {
        $this->validate($request);

        return $this->handle(
            new CreateCommand($request)
        );
    }

    public function update(User $user, UpdateUserRequest $request): User
    {
        $this->validate($request);

        return $this->handle(
            new UpdateCommand($user, $request)
        );
    }

    public function delete(User $user): void
    {
        $this->handle(
            new DeleteCommand($user)
        );
    }
}
