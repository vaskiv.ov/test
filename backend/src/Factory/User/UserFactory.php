<?php declare(strict_types=1);

namespace App\Factory\User;

use App\DTO\User\AbstractUserRequest;
use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserFactory
{
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher
    ) {}

    public function updateFromRequest(User $user, AbstractUserRequest $request): User
    {
        if ($request->plainPassword) {
            $password = $this->passwordHasher->hashPassword(
                $user,
                $request->plainPassword
            );

            $user->setPassword($password);
        }

        return $user->setFirstName($request->firstName)
            ->setLastName($request->lastName)
            ->setPosition($request->position)
            ->setEmail($request->email)
            ->setActive($request->active);
    }
}