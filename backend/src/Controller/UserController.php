<?php declare(strict_types=1);

namespace App\Controller;

use App\Application\Manager\UserManager;
use App\DTO\QueryParams;
use App\DTO\User\CreateUserRequest;
use App\DTO\User\ListResponse;
use App\DTO\User\UpdateUserRequest;
use App\Entity\User;
use App\Service\Cache\Attribute\CacheClear;
use App\Service\Cache\Attribute\CachedResponse;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

#[
    OA\Tag(name: 'API'),
    OA\Response(
        response: 404,
        description: 'Resource not found',
        content: new OA\JsonContent(
            properties: [
                new OA\Property(property: 'code', description: 'Error code', type: 'integer'),
                new OA\Property(property: 'message', description: 'Error description', type: 'string'),
            ],
            type: 'object'
        )
    ),
    Security(name: 'Bearer')
]
class UserController
{
    public function __construct(
        private readonly UserManager $manager
    ) {}

    /**
     * get users list
     */
    #[
        IsGranted("ROLE_USER"),
        Rest\Get("/api/user"),
        ParamConverter("queryParams", class: QueryParams::class, converter: "query_param_converter"),
        Rest\View(
            statusCode: 200,
            serializerGroups: ['userGet']
        ),
        OA\Parameter(
            name: 'queryParams',
            description: "Query parameters.",
            in: 'query',
            schema: new OA\Schema(ref: new Model(type: QueryParams::class))
        ),
        OA\Response(
            response: 200,
            description: 'Returns users list',
            content: new OA\JsonContent(
                ref: new Model(type: ListResponse::class, groups: ['userGet']),
            )
        ),
        CachedResponse('users_list', ['user'])
    ]
    public function getList(QueryParams $queryParams): ListResponse
    {
        return $this->manager->getList($queryParams);
    }

    /**
     * get user details
     */
    #[
        IsGranted("ROLE_USER"),
        Rest\Get("/api/user/{user}"),
        ParamConverter('user', class: User::class),
        Rest\View(
            statusCode: 200,
            serializerGroups: ['userGet']
        ),
        OA\Parameter(
            name: 'user',
            description: "User ID.",
            in: 'path',
            required: true,
            schema: new OA\Schema(type: 'int')
        ),
        OA\Response(
            response: 200,
            description: 'Returns user details',
            content: new OA\JsonContent(
                ref: new Model(type: User::class, groups: ['userGet']),
            )
        ),
        CachedResponse('user_get', ['user'])
    ]
    public function getUser(User $user): User
    {
        return $user;
    }

    /**
     * create new user
     */
    #[
        IsGranted("ROLE_USER"),
        Rest\Post("/api/user"),
        ParamConverter("request", converter: "fos_rest.request_body"),
        Rest\View(
            statusCode: 201,
            serializerGroups: ['userGet']
        ),
        OA\Response(
            response: 201,
            description: 'Returns created user details',
            content: new OA\JsonContent(
                ref: new Model(type: User::class, groups: ['userGet']),
            )
        ),
        CacheClear(['user'])
    ]
    public function createUser(CreateUserRequest $request): User
    {
        return $this->manager->create($request);
    }

    /**
     * update existed user
     */
    #[
        IsGranted("ROLE_USER"),
        Rest\Put("/api/user/{user}"),
        ParamConverter('user', class: User::class),
        ParamConverter("request", converter: "fos_rest.request_body"),
        Rest\View(
            statusCode: 200,
            serializerGroups: ['userGet']
        ),
        OA\Parameter(
            name: 'user',
            description: "User ID.",
            in: 'path',
            required: true,
            schema: new OA\Schema(type: 'int')
        ),
        OA\Response(
            response: 200,
            description: 'Returns updated user details',
            content: new OA\JsonContent(
                ref: new Model(type: User::class, groups: ['userGet']),
            )
        ),
        CacheClear(['user'])
    ]
    public function updateUser(User $user, UpdateUserRequest $request): User
    {
        return $this->manager->update($user, $request);
    }

    /**
     * delete existed user
     */
    #[
        IsGranted("ROLE_USER"),
        Rest\Delete("/api/user/{user}"),
        ParamConverter('user', class: User::class),
        Rest\View(
            statusCode: 204
        ),
        OA\Parameter(
            name: 'user',
            description: "User ID.",
            in: 'path',
            required: true,
            schema: new OA\Schema(type: 'int')
        ),
        OA\Response(
            response: 204,
            description: 'User deleted successfully',
        ),
        CacheClear(['user'])
    ]
    public function deleteUser(User $user): void
    {
        $this->manager->delete($user);
    }
}
