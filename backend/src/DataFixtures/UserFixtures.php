<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher,
    ) {}

    public function load(ObjectManager $manager): void
    {
        foreach (self::getData() as $item) {
            $user = (new User())
                ->setFirstName($item['firstName'])
                ->setLastName($item['lastName'])
                ->setPosition($item['position'])
                ->setEmail($item['email'])
                ->setPlainPassword($item['plainPassword'])
                ->setActive($item['active']);

            $user = $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPlainPassword()));

            $manager->persist($user);
            $manager->flush();
        }
    }

    private static function getData(): array
    {
        return [
            [
                'firstName' => 'John',
                'lastName' => 'Dow',
                'position' => 'Administrator',
                'email' => 'admin@admin.com',
                'plainPassword' => 'admin',
                'active' => true
            ],
            [
                'firstName' => 'Adam',
                'lastName' => 'Smith',
                'position' => 'Manager',
                'email' => 'manager1@manager.com',
                'plainPassword' => 'manager',
                'active' => true
            ],
            [
                'firstName' => 'John',
                'lastName' => 'Smith',
                'position' => 'Manager',
                'email' => 'manager2@manager.com',
                'plainPassword' => 'manager',
                'active' => true
            ],
            [
                'firstName' => 'Adam',
                'lastName' => 'Dow',
                'position' => 'Manager',
                'email' => 'manager3@manager.com',
                'plainPassword' => 'manager',
                'active' => false
            ]
        ];
    }
}
