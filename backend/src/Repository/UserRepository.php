<?php declare(strict_types=1);

namespace App\Repository;

use App\DTO\QueryParams;
use App\DTO\User\ListResponse;
use App\Entity\User;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

class UserRepository extends AbstractRepository implements UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function loadUserByIdentifier(string $email): ?User
    {
        return $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllPaginated(QueryParams $queryParams): ListResponse
    {
        $query = $this->createQueryBuilder('u')
            ->getQuery();

        $paginator = new Paginator($query);
        $total = $paginator->count();

        $result = $paginator->getQuery()
            ->setFirstResult($queryParams->limit * ($queryParams->page - 1))
            ->setMaxResults($queryParams->limit)
            ->getResult();

        return new ListResponse($result, $total);
    }
}