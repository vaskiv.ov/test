<?php declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

abstract class AbstractRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, string $entityClass)
    {
        parent::__construct($registry, $entityClass);
    }

    public function save(mixed $entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }

    public function delete(mixed $entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    public function beginTransaction(): void
    {
        $this->_em->beginTransaction();
    }

    public function rollback(): void
    {
        $this->_em->rollback();
    }

    public function commit(): void
    {
        $this->_em->commit();
    }
}