<?php declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

final class ConstraintValidatorException extends \Exception
{
    const HTTP_CODE = Response::HTTP_BAD_REQUEST;

    public function __construct(
        private readonly ConstraintViolationListInterface $constraintViolationList,
        protected $message = "Validation Error."
    )
    {
        parent::__construct($message);
    }

    public function getHttpCode(): int
    {
        return self::HTTP_CODE;
    }
    public function getConstraintViolationList(): ConstraintViolationListInterface
    {
        return $this->constraintViolationList;
    }
}
