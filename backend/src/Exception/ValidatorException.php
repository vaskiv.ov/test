<?php declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ValidatorException extends HttpException
{
    const HTTP_CODE = Response::HTTP_BAD_REQUEST;

    public function __construct(string $message)
    {
        parent::__construct(self::HTTP_CODE, $message);
    }
}
