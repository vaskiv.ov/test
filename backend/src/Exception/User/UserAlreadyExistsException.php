<?php declare(strict_types=1);

namespace App\Exception\User;

use App\Exception\ValidatorException;

final class UserAlreadyExistsException extends ValidatorException
{
    public function __construct()
    {
        parent::__construct('User already exists');
    }
}