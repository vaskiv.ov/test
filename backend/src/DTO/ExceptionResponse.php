<?php declare(strict_types=1);

namespace App\DTO;

final class ExceptionResponse
{
    public string $message;
    public array $details = [];
    private int $httpCode;

    public function __construct($httpCode, string $message, array $details = [])
    {
        $this->httpCode = $httpCode;
        $this->message = $message;
        $this->details = $details;
    }

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }
}