<?php declare(strict_types=1);

namespace App\DTO\User;

use JMS\Serializer\Annotation as Serializer;

final class ListResponse
{
    #[
        Serializer\Type('array<App\Entity\User>'),
        Serializer\Groups(['userGet'])
    ]
    public array $items = [];

    #[
        Serializer\Type('int'),
        Serializer\Groups(['userGet'])
    ]
    public int $total = 0;

    public function __construct(array $items, int $total)
    {
        $this->items = $items;
        $this->total = $total;
    }
}