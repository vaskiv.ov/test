<?php declare(strict_types=1);

namespace App\DTO\User;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateUserRequest extends AbstractUserRequest
{
    #[
        Assert\NotBlank(),
        Assert\Length(min: 6, max: 50),
        Assert\Type('string'),
        Serializer\Type('string')
    ]
    public ?string $plainPassword = null;
}