<?php declare(strict_types=1);

namespace App\DTO\User;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

abstract class AbstractUserRequest
{
    #[
        Assert\NotBlank(),
        Assert\Length(min: 2, max: 50),
        Assert\Type('string'),
        Serializer\Type('string')
    ]
    public string $firstName;

    #[
        Assert\NotBlank(),
        Assert\Length(min: 2, max: 50),
        Assert\Type('string'),
        Serializer\Type('string')
    ]
    public string $lastName;

    #[
        Assert\Length(min: 2, max: 50),
        Assert\Type('string'),
        Serializer\Type('string')
    ]
    public ?string $position = null;

    #[
        Assert\NotBlank(),
        Assert\Email(),
        Assert\Length(min: 2, max: 50),
        Assert\Type('string'),
        Serializer\Type('string')
    ]
    public string $email;

    #[
        Assert\NotBlank(),
        Assert\Type('bool'),
        Serializer\Type('bool')
    ]
    public bool $active;

    #[
        Assert\Length(min: 6, max: 50),
        Assert\Type('string'),
        Serializer\Type('string')
    ]
    public ?string $plainPassword = null;
}