<?php declare(strict_types=1);

namespace App\DTO;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class QueryParams
{
    #[
        Assert\Type('int'),
        Assert\GreaterThanOrEqual(1),
        Serializer\Type('int')
    ]
    public int $page = 1;

    #[
        Assert\Type('int'),
        Assert\LessThanOrEqual(100),
        Assert\GreaterThanOrEqual(1),
        Serializer\Type('int')
    ]
    public int $limit = 10;
}